angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
  });
})

.controller('StartCtrl', function($scope, $state, $http, $timeout) {
    $scope.go = function(state) {
    $state.go(state)

    }
    $scope.ini = function() {
      $http({
        method: "GET",
        url: "https://patrickhofer.ch/pois.php"
      }).then(function mySucces(response) {
        console.log("success");
      }, function myError(response) {
        $state.go("error");
      });
    }
})
.controller('AboutCtrl', function($scope, $state, $http, $timeout) {
    $scope.go = function(state) {
    $state.go(state)

    }
})

.controller('WordsCtrl', function($scope, $state, $http, $timeout, $ionicPopup, $ionicLoading) {
    $scope.go = function(state) {
    $state.go(state)

    }


    $ionicLoading.show({
      template: 'Ei Momänt bitte...'
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });

    $timeout(function() {
        $ionicLoading.hide().then(function(){
           console.log("The loading indicator is now hidden");
        });
    }, 4000)



  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };
  var dataAr = [];

  $scope.search = { word: '' };

firebase.database().ref("words/").on('value', function(snapshot) {
  snapshot.forEach(function(childSnapshot) {
        $scope.data = [childSnapshot.val()];
        $scope.locations = $scope.data
        $scope.locations.forEach(function(item) {


        dataAr.push(item);

        $timeout(function() {
          $scope.locations = dataAr;
        })

        })


      })
  })

})



.controller('HomeCtrl', function($scope, $state, $http, $timeout) {

    $scope.go = function(state) {
    $state.go(state)

    }


  $scope.getWord = function() {

      firebase.database().ref("entries/").on('value', function(snapshot) {
        $scope.blah = snapshot.val();

        var howmany = $scope.blah.entries

    var ran = Math.floor((Math.random() * howmany) + 1);
    $scope.rannum = Math.floor((Math.random() * 100) + 1);
    console.log(ran);

  firebase.database().ref("words/" + ran).on('value', function(snapshot) {

      $timeout(function() {
        $scope.tmp = snapshot.val();
        $scope.content = [$scope.tmp];
        console.log($scope.content);

    })

    });

  });

  }


})


.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('home', {
    url: '/home',
    templateUrl: 'templates/home.html',
    controller: "HomeCtrl"
  })
  .state('start', {
    url: '/start',
    templateUrl: 'templates/start.html',
    controller: "StartCtrl",
    cache: false
  })
  .state('words', {
    url: '/words',
    templateUrl: 'templates/words.html',
    controller: "WordsCtrl"
  })
  .state('about', {
    url: '/about',
    templateUrl: 'templates/about.html',
    controller: "AboutCtrl"
  })
  .state('error', {
    url: '/error',
    templateUrl: 'templates/error.html',
    controller: "AboutCtrl"
  })
  $urlRouterProvider.otherwise('start');

})

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.views.transition('none');
});
